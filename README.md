# sysroot

<b>sysroot</b> is a container build system that does two things:
- `sysroot` makes a container and creates a basic AutoSD-style OS environment in the container to do things natively.
- `sysroot` uses that basic container environment to properly (natively) collect and tar a sysroot filesystem of libs and includes which are suitable for use elsewhere by a cross-compiler.

The common use case is to collect a set of includes/libraries for aarch64, even though the container host is x86_64.

## Why sysroot exists

The AutoSD build system already builds and composes complete bootable images that can include arbitrary rpms, including compilers and all -devel packages (header includes and shared libraries).  So why does this utility exist?  The answer is:  In some preexisting workflows, the AutoSD compilers, libraries, and headers need to be extracted so various combinations of them can be used by:
- other cross compiler systems (such as Bootlin Buidroot) or preexisting highly-customized build systems
- source code analysis tools

sysroot creates part of that solution:  The partial rootfs that contains the -devel items which are built and linked to by a gcc-based toolchain.

## prerequisites

- <b>`qemu-user-static`</b>: If building a non-native container, then the appropriate qemu static support is required
  - Example: building aarch64 on x86_64 requires `qemu-aarch64-static` on the container host
- <b>`podman-5.0+`</b>: needed to automatically invoke the correct qemu-user-static system to run the container
  - Older versions of podman are OK when running native, which is not the usual use case.

## prebuilt versions of the container

This system does not offer prebuit container images in any registry (such as available from quay.io).  You must build the container locally using the Makefile targets in this repo.

## building the sysroot container

This example is for building an aarch64 sysroot when the container is hosted on x86_64:
- `git clone https://gitlab.com/bbensonredhat/sysroot.git`
- `make all` - builds the container, assembles the sysroot, runs a short test, and exits

## using sysroot

- `make pack` - Starts the container, rolls a tarball of the sysroot as `sysroot.gz` into the pwd where the container was run, and exits.  The tarball is now available in the directory where it was run.
- `mv` the `sysroot.gz` tarball to the desired location for further use, 
- unpack with `tar -xzvf sysroot.gz`, and 
- point to it with your cross-compiler

The make targets generally default toward building an aarch64 sysroot, but other make targets exist or can be created in the Makefile.

## other things the container can do
- `make test` - ensures the container runs, and exit
- `make run` - starts up the container with an interactive shell to enable more libraries to be added
- `make help` - show available make targets

## License

GPLv2

## Authors and acknowledgment

This is based on work from AutoSD utility containers, especially [autosd-buildbox](https://gitlab.com/CentOS/automotive/container-images/autosd-buildbox) and [autosd-buildbox-x86_64-to-aarch64-cross-tools](https://gitlab.com/CentOS/automotive/container-images/autosd-buildbox-x86_64-to-aarch64-cross-tools).

Contributors include Leonardo Rossetti, Stephen Smoogen, and Lester Claudio.
