NAME ?= sysroot
TAG ?= latest
CONTAINER ?= $(NAME):$(TAG)

REGISTRY ?= localhost
#UPLOADREGISTRY ?= quay.io/centos-sig-automotive
UPLOADREGISTRY ?= quay.io/rhn-gps-bbenson

SYSROOTDIR := /usr/aarch64-redhat-linux/sys-root/el9

TESTCOMMAND := "set -e; echo -n 'arch: '; arch; echo 'sysroot='${SYSROOTDIR}; ls ${SYSROOTDIR}; "
#TESTCOMMAND := "set -e; echo 'libs test: '; LD_DEBUG=libs ${SYSROOTDIR}/lib/ld-linux-aarch64.so.1 ${SYSROOTDIR}/usr/bin/true; "
#TESTCOMMAND := "set -e; echo '* Arm64 GCC: '; aarch64-redhat-linux-gcc --version; "
#TESTCOMMAND := "set -e; echo '* Arm64 GCC: '; aarch64-redhat-linux-gcc -dumpmachine; "
#TESTCOMMAND := "set -e; echo '* Arm64 GCC: '; aarch64-redhat-linux-gcc -xc -E -v; "

PACKCOMMAND := "set -e; cd ${SYSROOTDIR} ; tar -czvf /var/tmp/sysroot.gz *; mv /var/tmp/sysroot.gz $$(pwd)/; "

##@ Help-related tasks
.PHONY: help
help: ## Help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^(\s|[a-zA-Z_0-9-])+:.*?##/ { printf "  \033[36m%-35s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ Build-related tasks
.PHONY: all
all: manifest build test ## Build the container locally (all arches) and print installed test

.PHONY: manifest
manifest: ## creates the buildah manifest for multi-arch images
	buildah manifest create "${REGISTRY}/${CONTAINER}"

.PHONY: build
build: build-podman ## Build the container locally (all arches) and print installed test

.PHONY: build-podman
build-podman: build-podman-arm64 ## Build amd64

.PHONY: build-podman-amd64
build-podman-amd64: ## build the container in amd64
	@echo "Building the utility container amd64"
	buildah bud --arch=amd64 --build-arg TARGETARCH=amd64 --build-arg ALTTARGETARCH=x86_64 \
		--build-arg OPTTARGETARCH='' --build-arg EXTRARPMS='' --format docker \
		--force-rm=true --no-cache \
		-f Containerfile -t "${CONTAINER}-amd64"
	buildah manifest add --arch=amd64 "${REGISTRY}/${CONTAINER}" "${REGISTRY}/${CONTAINER}-amd64"

.PHONY: build-podman-arm64
build-podman-arm64: ## build the container in arm64
	@echo "Building the utility container arm64"
	buildah bud --arch=arm64 --build-arg TARGETARCH=arm64 \
		--build-arg OPTTARGETARCH='' --build-arg EXTRARPMS='' --format docker \
		--force-rm=true --no-cache \
		-f Containerfile -t "${CONTAINER}-arm64"
	buildah manifest add --arch=arm64 "${REGISTRY}/${CONTAINER}" "${REGISTRY}/${CONTAINER}-arm64"

##@ Runtime-related tasks
.PHONY: test-amd64
test-amd64: ## Prints the test inside the container amd64
	@echo "** WIP: Testing linux/amd64"
	@podman run --arch=amd64 --rm -it --net=host "${REGISTRY}/${CONTAINER}-amd64" bash -c \
		$(TESTCOMMAND)

.PHONY: test-arm64
test-arm64: ## Prints the test inside the container arm64
	@echo "** Testing linux/arm64"
	@podman run --rm -it --arch=arm64 \
		--net=host \
		--security-opt label=disable \
		-v ${PWD}:${PWD} \
		-w $$(pwd) \
		"${REGISTRY}/${CONTAINER}-arm64" bash -c \
		$(TESTCOMMAND)

.PHONY: test
test: test-arm64 ## Tests the container for all the required bits

.PHONY: run-podman-native
run-podman-native: ## Runs the container interactively
	podman run --rm -it --net=host \
		--security-opt label=disable \
		-v ${HOME}:/pattern \
		-v ${HOME}:${HOME} \
		-w $$(pwd) "${REGISTRY}/${CONTAINER}" sh

.PHONY: run-podman-amd64
run-podman-amd64: ## Runs the container interactively
	podman run --rm -it --arch amd64 \
	        --net=host \
		--security-opt label=disable \
		-v ${HOME}:/pattern \
		-v ${HOME}:${HOME} \
		-w $$(pwd) "${REGISTRY}/${CONTAINER}-amd64" sh

.PHONY: run-podman-arm64
run-podman-arm64: ## Runs the container interactively
	podman run --rm -it --arch arm64 \
	        --net=host \
		--security-opt label=disable \
		-v ${PWD}:${PWD} \
		-v ${HOME}:${HOME} \
		-w $$(pwd) "${REGISTRY}/${CONTAINER}-arm64" sh

.PHONY: run
run: run-podman-arm64 #select a default run

.PHONY: pack-arm64
pack-arm64: ## pack up the sysroot and put it in a shared place
	@echo "** pack the sysroot into the shared /var/tmp"
	@podman run --arch=arm64 --rm -it \
		--net=host \
		--security-opt label=disable \
		-v ${PWD}:${PWD} \
	        -w $$(pwd) "${REGISTRY}/${CONTAINER}-arm64" bash -c \
		$(PACKCOMMAND)

.PHONY: pack
pack: pack-arm64 #select a default run for pack

##@ Maintenance-related tasks
.PHONY: upload
upload: ## Uploads the container to ${UPLOADREGISTRY}/${CONTAINER}
	@echo "Uploading the ${REGISTRY}/${CONTAINER} container to ${UPLOADREGISTRY}/${CONTAINER}"
	buildah manifest push --all "${REGISTRY}/${CONTAINER}" "docker://${UPLOADREGISTRY}/${CONTAINER}"

.PHONY: clean
clean: ## Removes any previously built artifact
	buildah manifest rm "${REGISTRY}/${CONTAINER}"

