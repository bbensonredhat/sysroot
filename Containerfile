# sysroot is intended to be built from this Containerfile via the buildah bud invoked in the Makefile
# This container is largely arch-agnostic and can be build under any arch
# However a separate sysroot is created for a desired target arch of a cross-compiler
# the most common use case is building this container as arm64 while aboard an x86_64 container host
# which requires qemu-aarch64-static

#ARG

#ENV

#FROM quay.io/centos-sig-automotive/autosd-buildbox:latest
FROM quay.io/centos/centos:stream9
#FROM quay.io/centos/centos:stream9-minimal

LABEL com.github.containers.toolbox="true" \
      com.redhat.component="$NAME" \
      name="$NAME" \
      version="$VERSION" \
      usage="This image can be used podman run --arch" \
      summary="Base sysroot image for building with cross compilation tools" \
      maintainer="Bruce Benson <bbenson@redhat.com>" \
      authors="Leonardo Rossetti, Stephen Smoogen, Lester Claudio, Bruce Benson"

#ENTRYPOINT 

#CMD

#### BASE OS PREP
RUN dnf update -y && \
    dnf install -y 'dnf-command(config-manager)' epel-release

# enable crb and autosd repositories
RUN dnf config-manager --set-enabled crb && \
    dnf config-manager --add-repo https://buildlogs.centos.org/9-stream/automotive/$(arch)/packages-main/ && \
    dnf config-manager --add-repo https://buildlogs.centos.org/9-stream/autosd/$(arch)/packages-main/

# copy configuration files to the filesystem to do things inside the container during build
COPY rpms.in /rpms.in
COPY rpms-installroot.in /rpms-installroot.in

# install additional packages as desired
RUN dnf -y --skip-broken --best --allowerasing install $(cat /rpms.in)
RUN dnf update -y

#### BASE OS READY
# at this point, there is a basic system to navigate around in under whatever arch this continer was built to

#### ASSEMBLE SYSROOT
# add sysroot in well-known location for includes and libraries
# This method uses installation from rpms of the target arch
# via forcearch, even if host is same arch
RUN mkdir -pv /usr/aarch64-redhat-linux/sys-root/el9 && \
    dnf --forcearch=aarch64 \
    --installroot=/usr/aarch64-redhat-linux/sys-root/el9 \
    --nogpgcheck \
    --releasever=9 \
    install -y $(cat rpms-installroot.in)

# cleanup
RUN dnf clean all

#ONBUILD
